// Bai 1

function inBang(){
    var ketqua = "";
    let Arr = [];
   
    for(y =1 ; y <= 10 ; y ++){
        Arr[y] = [] 
        for(x = 1; x <=100; x++){
        
            if(x <= y*10 && x > (y-1)*10){
                Arr[y].push(x)
            }
        }
        Arr[y].join('\n')
    }

    ketqua = Arr.map(item => item + '</br>').join('\n')
    document.querySelector(".ketQua1").innerHTML = ketqua
}
inBang()

// Bai 2
let arrSoNguyen = [];

function themMang(idSoNguyen,idClick){
    let soNguyenEl = document.querySelector(idSoNguyen);
    let click = document.querySelector(idClick);
    click.addEventListener("click",function(){
        let soNguyen = soNguyenEl.value*1;
        arrSoNguyen.push(soNguyen)
        document.querySelector(".arr").innerHTML = arrSoNguyen;
    })
};

themMang(".nhapSoNguyen",".themSoNguyen");


let ketqua = "";
function timSoNguyenTo(soNguyen){
    let timSoNguyenTo = document.querySelector(".timSoNT");
    timSoNguyenTo.addEventListener("click", function(){
        for(i=1; i< soNguyen.length; i++){
            let lisNumner = soNguyen[i];
            let flag = true;
            for(x = 2; x <= Math.sqrt(lisNumner); x++){
                if(lisNumner % x ===0){
                    flag = false;
                    break;
                }
            }
            if(flag == true){
                ketqua = ketqua + lisNumner + ", ";
            }
        }
        document.querySelector(".ketQua2").innerHTML = ketqua;
    })
}

timSoNguyenTo(arrSoNguyen)

// Bai3

function tinhN(idnumber,idClick){
    let number =  document.querySelector(idnumber);
    let click = document.querySelector(idClick)
    click.addEventListener("click",function(){
        let Number = number.value*1
        let m = 0;
        let tong = 0;
        for(i = 2; i<=Number; i++){
            let S = (m += i)+2*Number
            console.log(S)
            tong = S
        }
        document.querySelector(".ketQua3 span").innerHTML = tong;
    })
}
tinhN(".soN",".tinh3")


// Bai 4

function tinhUocSo(idNumber,idTinh){
    let numberEl = document.querySelector(idNumber);
    let tinh = document.querySelector(idTinh);
    tinh.addEventListener("click",function(){
        let number = numberEl.value*1;
        let uocSo = "";
        for (i = 0; i<= number; i++){
            if(number % i === 0){
                console.log(i)
                uocSo += i + ", "
            }
        }
        document.querySelector(".ketQua4 span").innerHTML = `Ước số của ${number} là: ${uocSo}`
    })
}

tinhUocSo(".nhapSoN",".tinh4");


// Bai 5

function daoNguocSoNguyen(idNumber, idClick,ketQua){
    let numberEl = document.querySelector(idNumber);
    let click = document.querySelector(idClick);
    click.addEventListener("click",function(){
        let number = numberEl.value;
        let daoSo = ""
        for(let i= number.length-1; i>=0; i--){
            daoSo += number[i];
        }
        console.log(daoSo)
        document.querySelector(ketQua).innerHTML = daoSo;
    })
}

daoNguocSoNguyen(".nhapSoNguyenDuong", ".tinh5", ".ketQua5 span")


// Bai 6
function timSoLonNhat(){
    let tong = 0;
    for(i = 1; i<=1000;i++){
        tong +=i
        if(tong <=100){
            document.querySelector(".ketQua6 span").innerHTML = `Số x lớn nhất là: ${i}.`;
        }
    }
};

// Bài 7

function inBangCuuChuong(idNumber, idClick){
    let numberEl = document.querySelector(idNumber)
    let click = document.querySelector(idClick);
    click.addEventListener("click", function(){
        let number = numberEl.value*1
        let ketQua = "";
        for (i=1;i<=1;i++){
            for(j = 0; j<=10;j++){
                ketQua += number + " X " + j + " = " + (number*j) + `</br>`;
            }
        }
        document.querySelector(".ketQua7").innerHTML = ketQua;
    })
}

inBangCuuChuong(".nhapVaoSoN", ".tinh7")

// Bai 9

function tinhSoConGaVaCho(idSoCon,idSoChan,idClick) {
    let soConEl = document.querySelector(idSoCon);
    let soChanEl = document.querySelector(idSoChan);
    let click = document.querySelector(idClick);
    click.addEventListener("click", function(){
        let m = soConEl.value*1;
        let n = soChanEl.value*1;
        // m là tổng số con vd: 36
        // n là tổng số chân vd: 100
        // a là gà
        // b là cho
        let b = (n - 2*m)/2
        let a = m - b
        document.querySelector(".ketQua9 span").innerHTML = `${m} con, ${n} chân. Trong đó có ${a} con gà, và ${b} con chó`;
    })
}

tinhSoConGaVaCho(".nhapSoCon",".nhapSoChan",".tinh9")


// Bài 10

function tinhGoc(idSoGio, idSoPhut, idClick){
    let soGioEl = document.querySelector(idSoGio);
    let soPhutEl = document.querySelector(idSoPhut);
    let click = document.querySelector(idClick);
    click.addEventListener("click", function(){
        let soGio = soGioEl.value*1;
        let soPhut = soPhutEl.value*1;
        let doPhut = soPhut*6
        let doGio = (soGio*60 + soPhut)*0.5;
        let gocLech = Math.abs(doPhut - doGio);
        document.querySelector(".ketQua10").innerHTML = `Góc lệnh của hai kim khi ở ${soGio}:${soPhut} là: ${gocLech} độ`;
    })
}
tinhGoc(".soGio", ".soPhut", ".tinh10")
